package com.rojoxpress.mockbnb.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.rojoxpress.mockbnb.models.Place;
import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.ui.activities.PlaceDetailActivity;
import com.rojoxpress.mockbnb.databinding.ItemPlaceBinding;
import com.rojoxpress.mockbnb.ui.components.VolleyImageView;
import com.rojoxpress.mockbnb.utils.Utils;

import java.util.ArrayList;

public class PlacesPagerAdapter extends PagerAdapter {

    private AppCompatActivity activity;
    private ArrayList<Place> places;

    public PlacesPagerAdapter(AppCompatActivity context, ArrayList<Place> places){
        this.places = places;
        this.activity = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {

        final ItemPlaceBinding item = DataBindingUtil.bind(
                View.inflate(activity, R.layout.item_place,null));
        item.setPlace(places.get(position));
        collection.addView(item.getRoot());
        item.placeImage.setResponseObserver(new VolleyImageView.ResponseObserver() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess() {
                Utils.scheduleStartPostponedTransition(item.placeImage,activity);
            }
        });

        for(int i=0;i<5;i++){
            ImageView imageView = new ImageView(activity);
            imageView.setImageResource(R.drawable.ic_star);
            int color = i < places.get(position).getRating() ?  R.color.star_yellow : R.color.shadow;
            imageView.setColorFilter(ContextCompat.getColor(activity,color));
            int size = Utils.dpToPixels(30,activity);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(size,size));
            item.ratingCount.addView(imageView);
        }

        item.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = Bitmap.createBitmap(item.placeImage.getWidth(), item.placeImage.getHeight(),
                        Bitmap.Config.ARGB_8888);
                Bundle b;
                Intent intent = new Intent(activity,PlaceDetailActivity.class);
                intent.putExtra("place",places.get(position));
                if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                    item.placeImage.setTransitionName("image");
                    Pair<View, String> p1 = Pair.create(item.getRoot().findViewById(R.id.place_image), "image");
                    Pair<View, String> p2 = Pair.create(item.getRoot().findViewById(R.id.place_price), "price");
                    Pair<View, String> p3 = Pair.create(item.getRoot().findViewById(R.id.rating_count), "rating");
                    b=  ActivityOptionsCompat.
                            makeSceneTransitionAnimation(activity,p1,p2,p3).toBundle();
                }else {
                    b = ActivityOptionsCompat.makeThumbnailScaleUpAnimation(item.placeImage, bitmap, 0, 0).toBundle();
                }
                ActivityCompat.startActivity(activity, intent, b);
            }
        });
        return item.getRoot();
    }

    @Override
    public int getCount() {
        return places.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }



    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
