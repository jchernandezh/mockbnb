package com.rojoxpress.mockbnb.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.models.City;

import java.util.ArrayList;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private Context context;
    private ArrayList<City> cities;
    private OnItemClickListener mItemClickListener;

    public CityAdapter(Context context,ArrayList<City> cities){

        this.context = context;
        this.cities = cities;
    }


    public final class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View convertView;
        private TextView title,description;

        public View getView(){
            return convertView;
        }


        public ViewHolder(View convertView,ViewGroup parent) {
            super(convertView);
            this.convertView = convertView;
            title = (TextView) convertView.findViewById(R.id.title);
            description = (TextView) convertView.findViewById(R.id.description);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null)
                mItemClickListener.onItemClick(v, getAdapterPosition());
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View view , int position);
    }

    public void setItemClickListener(OnItemClickListener listener){
        mItemClickListener=listener;
    }

    @Override
    public CityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_city_list, parent, false),parent);
    }

    @Override
    public void onBindViewHolder(final CityAdapter.ViewHolder holder, int position) {
        holder.title.setText(cities.get(position).getName());
        holder.description.setText(cities.get(position).getDescription());
    }


    @Override
    public int getItemCount() {
        return cities.size();
    }

    public void setCities(ArrayList<City> cities){
        this.cities = cities;
    }
}