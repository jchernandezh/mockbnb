package com.rojoxpress.mockbnb.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.rojoxpress.mockbnb.models.City;
import com.rojoxpress.mockbnb.ui.activities.MainActivity;
import com.rojoxpress.mockbnb.ui.adapters.CityPagerAdapter;
import com.rojoxpress.mockbnb.models.Place;
import com.rojoxpress.mockbnb.ui.adapters.PlacesPagerAdapter;
import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.databinding.FragmentHomeBinding;
import com.rojoxpress.mockbnb.databinding.RowDataBinding;
import com.rojoxpress.mockbnb.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class UniqueFragment extends Fragment {

    private FragmentHomeBinding binding;
    private ArrayList<City> recent, weekend;
    private ArrayList<Place> places;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home,container,false);
        ((AppCompatActivity )getActivity()).getSupportActionBar().setTitle(getArguments().getString("title"));
        setObjects();
        setViews();
        return binding.getRoot();
    }

    public void setObjects(){

        places = new ArrayList<>();
        recent = new ArrayList<>();
        weekend = new ArrayList<>();

        try {
            JSONObject jsonObject = loadJSONFromAsset();
            JSONArray recent = jsonObject.getJSONArray("recent");
            JSONArray seen = jsonObject.getJSONArray("seen");
            JSONArray weekend = jsonObject.getJSONArray("weekend");

            for(int i=0;i<recent.length();i++){
                JSONObject jCity = recent.getJSONObject(i);
                City city = new City();
                city.setName(jCity.getString("city_name"));
                city.setDate(jCity.getString("date"));
                city.setDescription(jCity.getString("description"));
                city.setImage(jCity.getString("image_url"));
                this.recent.add(city);
            }
            for(int i=0;i<seen.length();i++){
                JSONObject jPlace = seen.getJSONObject(i);
                Place place = new Place();
                place.setTitle(jPlace.getString("title"));
                place.setImage(jPlace.getString("rating"));
                place.setPrice(jPlace.getString("price"));
                place.setType(jPlace.getString("type"));
                place.setImage(jPlace.getString("image_url"));
                place.setRating(jPlace.getInt("rating"));
                place.setReviews(jPlace.getInt("reviews"));
                place.setDescription(jPlace.getString("description"));
                place.setCity(jPlace.getString("city"));
                places.add(place);
            }
            for(int i=0;i<weekend.length();i++){
                JSONObject jCity = weekend.getJSONObject(i);
                City city = new City();
                city.setName(jCity.getString("city_name"));
                city.setDate(jCity.getString("date"));
                city.setImage(jCity.getString("image_url"));
                city.setDescription(jCity.getString("description"));
                this.weekend.add(city);
            }

            if(getActivity() instanceof MainActivity){
                MainActivity activity = (MainActivity) getActivity();
                activity.onLoadData(this.weekend);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void setViews() {

        addCities(recent, "Búsquedas Recientes");
        addDivider();
        addPlaces(places, "Vistos Recientemente");
        addDivider();
        addCities(weekend,"Para el fin de semana");
    }

    public void addCities( ArrayList<City> cities,String title){
        RowDataBinding recentRow = DataBindingUtil.bind(
                View.inflate(getContext(),R.layout.row_data,null));
        recentRow.title.setText(title);
        recentRow.pager.setAdapter(new CityPagerAdapter((AppCompatActivity) getActivity(),cities));

        recentRow.pager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        recentRow.pager.setOffscreenPageLimit(3);

        binding.scrollContainer.addView(recentRow.getRoot());

    }

    public void addPlaces(ArrayList<Place> places,String title){
        RowDataBinding placesRaw = DataBindingUtil.bind(
                View.inflate(getContext(),R.layout.row_data,null));
        placesRaw.title.setText(title);
        placesRaw.pager.setAdapter(new PlacesPagerAdapter((AppCompatActivity) getActivity(),places));

        placesRaw.pager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        placesRaw.pager.setOffscreenPageLimit(3);

        binding.scrollContainer.addView(placesRaw.getRoot());
    }

    public void addDivider(){
        View view = new View(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPixels(1,getContext()));
        int margin = Utils.dpToPixels(10,getContext());
        params.setMargins(0,margin,0,margin);
        view.setLayoutParams(params);
        view.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.shadow));
        binding.scrollContainer.addView(view);
    }

    public JSONObject loadJSONFromAsset() {
        InputStream inputStream = getContext().getResources().openRawResource(R.raw.home_data);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
            return new JSONObject(byteArrayOutputStream.toString());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return  null;
        }
    }
}
