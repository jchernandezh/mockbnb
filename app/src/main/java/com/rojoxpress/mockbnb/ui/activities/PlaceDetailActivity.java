package com.rojoxpress.mockbnb.ui.activities;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.databinding.ActivityPlaceDetailBinding;
import com.rojoxpress.mockbnb.models.Place;
import com.rojoxpress.mockbnb.ui.components.ObservableScrollViewCallbacks;
import com.rojoxpress.mockbnb.ui.components.ScrollState;
import com.rojoxpress.mockbnb.utils.Utils;

public class PlaceDetailActivity extends AppCompatActivity implements ObservableScrollViewCallbacks {


    private ColorDrawable mColorDrawable;
    private ActivityPlaceDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportPostponeEnterTransition();
        binding = DataBindingUtil.setContentView(this,R.layout.activity_place_detail);
        Place place = (Place) getIntent().getSerializableExtra("place");
        binding.setPlace(place);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(place.getCity());

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            final int statusBarSize = Utils.getStatusBarHeight(this);
            binding.toolbar.post(new Runnable() {
                @Override
                public void run() {
                    int toolBarSize = binding.toolbar.getHeight() + statusBarSize;
                    FrameLayout.LayoutParams params =
                            new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,toolBarSize);
                    binding.toolbar.setLayoutParams(params);
                    binding.toolbar.setPadding(0,statusBarSize,0,0);
                }
            });
        }

        mColorDrawable = new ColorDrawable(ContextCompat.getColor(this,R.color.colorPrimary));
        mColorDrawable.setAlpha(80);
        binding.toolbar.setBackgroundDrawable(mColorDrawable);

        binding.scrollView.setScrollViewCallbacks(this);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            binding.topImage.setTransitionName("image");
            binding.placePrice.setTransitionName("price");
            binding.ratingCount.setTransitionName("rating");
            binding.topImage.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            binding.topImage.getViewTreeObserver().removeOnPreDrawListener(this);
                            supportStartPostponedEnterTransition();
                            return true;
                        }
                    });
        }

        for(int i=0;i<5;i++){
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.ic_star);
            int color = i < place.getRating() ?  R.color.star_yellow : R.color.shadow;
            imageView.setColorFilter(ContextCompat.getColor(this,color));
            int size = Utils.dpToPixels(30,this);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(size,size));
            binding.ratingCount.addView(imageView);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int headerHeight = binding.topImage.getHeight() + binding.toolbar.getHeight();
        float ratio = (float) Math.min(Math.max(scrollY, 0), headerHeight) / headerHeight;
        int newAlpha = (int) (ratio * 255);
        mColorDrawable.setAlpha(newAlpha);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
}
