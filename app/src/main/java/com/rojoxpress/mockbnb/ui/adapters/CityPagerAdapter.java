package com.rojoxpress.mockbnb.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.rojoxpress.mockbnb.models.City;
import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.ui.activities.CityDetailActivity;
import com.rojoxpress.mockbnb.ui.components.VolleyImageView;
import com.rojoxpress.mockbnb.utils.Utils;
import com.rojoxpress.mockbnb.databinding.ItemCityBinding;

import java.util.ArrayList;

public class CityPagerAdapter extends PagerAdapter {

    private AppCompatActivity activity;
    private ArrayList<City> cities;

    public CityPagerAdapter(AppCompatActivity activity, ArrayList<City> cities){
        this.activity = activity;
        this.cities = cities;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {

        final ItemCityBinding item = DataBindingUtil.bind(
                View.inflate(activity, R.layout.item_city,null));

        item.setCity(cities.get(position));
        item.backImage.setResponseObserver(new VolleyImageView.ResponseObserver() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess() {
                Utils.scheduleStartPostponedTransition(item.backImage,activity);
            }
        });
        item.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = Bitmap.createBitmap(item.backImage.getWidth(), item.backImage.getHeight(),
                        Bitmap.Config.ARGB_8888);
                Bundle b;
                Intent intent = new Intent(activity, CityDetailActivity.class);
                intent.putExtra("item",cities.get(position));
                if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                    item.backImage.setTransitionName("image");
                    Pair<View, String> p1 = Pair.create(item.getRoot().findViewById(R.id.back_image), "image");
                    b=  ActivityOptionsCompat.
                            makeSceneTransitionAnimation(activity,p1).toBundle();
                }else {
                    b = ActivityOptionsCompat.makeThumbnailScaleUpAnimation(item.backImage, bitmap, 0, 0).toBundle();
                }
                ActivityCompat.startActivity(activity, intent, b);
            }
        });
        collection.addView(item.getRoot());

        return item.getRoot();
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }



    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
