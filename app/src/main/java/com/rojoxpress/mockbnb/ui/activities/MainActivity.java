package com.rojoxpress.mockbnb.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.models.City;
import com.rojoxpress.mockbnb.ui.fragments.UniqueFragment;
import com.rojoxpress.mockbnb.databinding.ActivityMainBinding;
import com.rojoxpress.mockbnb.utils.Utils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private ActivityMainBinding binding;
    private ArrayList<City> cities;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar( binding.content.toolbar);

        binding.content.fab.setOnClickListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout,  binding.content.toolbar
                , R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        binding.navView.setNavigationItemSelectedListener(this);
        onNavigationItemSelected(binding.navView.getMenu().getItem(2));
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        String title;
        if (id == R.id.nav_search) {
            if(cities != null) {
                Intent intent = new Intent(this, SearchActivity.class);
                intent.putExtra("cities", cities);
                startActivity(intent);
            }
            return true;
        } else if (id == R.id.nav_home) {
            title = "Inicio";
        } else if (id == R.id.nav_travel) {
            title = "Viajes";
        } else if (id == R.id.nav_wish) {
            title =  "Lista";
        } else {
            title = "about";
            Utils.openInChromeTabs(this, Uri.parse("http://branded.me/jc-hernandez"));
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);
        UniqueFragment fragment = new UniqueFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        fragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment,fragment);
        transaction.commit();
        return true;
    }

    public void onLoadData(ArrayList<City> cities){
        this.cities = cities;
    }

    @Override
    public void onClick(View v) {
        if(cities != null) {
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra("cities", cities);
            startActivity(intent);
        }
    }
}
