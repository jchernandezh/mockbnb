package com.rojoxpress.mockbnb.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.databinding.ActivitySearchBinding;
import com.rojoxpress.mockbnb.models.City;
import com.rojoxpress.mockbnb.ui.adapters.CityAdapter;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements TextWatcher, CityAdapter.OnItemClickListener {

    private ActivitySearchBinding binding;
    private ArrayList<City> cities, searchCities;
    private CityAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_search);
        cities = (ArrayList<City>) getIntent().getSerializableExtra("cities");
        searchCities = (ArrayList<City>) getIntent().getSerializableExtra("cities");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.cityList.setLayoutManager(layoutManager);
        binding.cityList.setAdapter(adapter = new CityAdapter(this,cities));
        adapter.setItemClickListener(this);

        binding.searchBar.getEditText().addTextChangedListener(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length()>0){
            ArrayList<City> searchCities = new ArrayList<>();
            for(int i=0;i<cities.size();i++){
                if(cities.get(i).getName().toLowerCase().contains(s.toString().toLowerCase())){
                   searchCities.add(cities.get(i));
                }
            }
            adapter.setCities(searchCities);
            adapter.notifyDataSetChanged();
        } else {
            adapter.setCities(cities);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(this, CityDetailActivity.class);
        intent.putExtra("item",cities.get(position));
        startActivity(intent);
    }
}
