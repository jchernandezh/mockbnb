package com.rojoxpress.mockbnb;

import android.app.Application;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.rojoxpress.mockbnb.utils.BitmapLruCache;

public class mApplication extends Application{

    public ImageLoader imageLoader;
    public BitmapLruCache bitmapLruCache;

    private static mApplication mInstance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        imageLoader = new ImageLoader(Volley.newRequestQueue(this),new BitmapLruCache());

    }

    public static mApplication getInstance(){
        return mInstance;
    }

    public ImageLoader getImageLoader(){
        return imageLoader;
    }
}
