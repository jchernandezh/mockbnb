package com.rojoxpress.mockbnb.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;

import com.android.volley.toolbox.NetworkImageView;
import com.rojoxpress.mockbnb.R;
import com.rojoxpress.mockbnb.mApplication;
import com.rojoxpress.mockbnb.ui.components.VolleyImageView;

public class Utils {
    /**
     * Method to convert Android dp to pixels depending on screen resolution
     *
     * @param dp      int of dp to convert
     * @param context Application Context
     * @return int converted pixels size
     */

    public static int dpToPixels(int dp, Context context) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    /**
     * Method to convert pixels to Android dp depending on screen resolution
     *
     * @param pixels  int number of pixels to convert
     * @param context Application Context
     * @return int converted dp size
     */
    public static int pixelsToDp(int pixels, Context context) {
        return (int) (pixels / context.getResources().getDisplayMetrics().density);

    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(final View view, String url) {
        mApplication application = mApplication.getInstance();

        if(view instanceof NetworkImageView){
            NetworkImageView netIV = (NetworkImageView) view;
            netIV.setDefaultImageResId(R.drawable.shadow_cover);
            netIV.setImageUrl(url,application.getImageLoader());
        } else if(view instanceof VolleyImageView){
            VolleyImageView netIV = (VolleyImageView) view;
            netIV.setDefaultImageResId(R.drawable.shadow_cover);
            netIV.setImageUrl(url,application.getImageLoader());
        }
    }

    public static void scheduleStartPostponedTransition(final View sharedElement, final AppCompatActivity activity) {
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        activity.supportStartPostponedEnterTransition();
                        return true;
                    }
                });
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    public static void  openInChromeTabs(Activity activity, Uri url){

        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
        CustomTabActivityHelper.openCustomTab(activity, customTabsIntent, url,
                new CustomTabActivityHelper.CustomTabFallback() {
                    @Override
                    public void openUri(Activity activity, Uri uri) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                        activity.startActivity(browserIntent);
                    }
                });

    }
}
